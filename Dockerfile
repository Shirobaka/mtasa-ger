FROM        ubuntu:16.04

LABEL       author="Shirobaka" maintainer="info@six-gaming.com"

RUN         dpkg --add-architecture i386 \
			&& apt update \
			&& apt upgrade -y \
			&& apt install -y libstdc++6 lib32stdc++6 libreadline5 libncursesw5 lib32ncursesw5 tar curl iproute2 openssl tzdata lib32gcc1 libstdc++6 libstdc++6:i386 zlib1g \
			&& useradd -d /home/container -m container \
			&& ln -snf /usr/share/zoneinfo/Europe/Berlin /etc/localtime && echo Europe/Berlin > /etc/timezone \
			&& dpkg-reconfigure --frontend noninteractive tzdata \
			&& ls -l /etc/localtime \
			&& curl -o /usr/lib/libmysqlclient.so.16 https://nightly.mtasa.com/files/x64/libmysqlclient.so.16

USER        container
ENV         USER=container HOME=/home/container

WORKDIR     /home/container

COPY        ./entrypoint.sh /entrypoint.sh

CMD ["/bin/bash", "/entrypoint.sh"]